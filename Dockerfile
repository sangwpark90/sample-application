FROM tomcat:7

RUN apt-get update -y
RUN apt-get upgrade -y

COPY target/DateTimeWebapp-1.0.war /usr/local/tomcat/webapps/app.war

EXPOSE 8080

CMD ["catalina","run"]
